# AppSec Command Line Utils

This project contains useful functions to make common tasks easier. Clone this somewhere on your machine and source the `appsec-utils.sh` file in your `.bashrc`, `.zshrc`, or wherever you'd like! The `update-appsec-utils` function will pull updates from the git repository.
